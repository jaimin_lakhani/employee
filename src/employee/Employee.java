/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package employee;

/**
 *
 * @author jaiminlakhani
 */
public class Employee {
    String name;
    double wage;
    double hours;
    
    Employee(String name, double wage, double hours) {
	this.name = name;
	this.wage = wage;
	this.hours = hours;
    }
    
    public String getName() {
        return name;
    }
    
    public double getWage() {
        return wage;
    }
    
    public double getHours() {
        return hours;
    }
    
    public double calculatePay () {
        return getWage() * getHours();
    }
}
