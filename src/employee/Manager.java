/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employee;

/**
 *
 * @author jaiminlakhani
 */
public class Manager extends Employee {
    double bonus;

    Manager(String name, double wage, double hours, double bonus) {
	super(name, wage, hours);
        this.bonus = bonus;
    }
    
    public double getBonus() {
        return bonus;
    }
    @Override
    public double calculatePay() {
	return super.calculatePay() + getBonus();
    }
}
