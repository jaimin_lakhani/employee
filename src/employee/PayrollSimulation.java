/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employee;

/**
 *
 * @author jaiminlakhani
 */
public class PayrollSimulation {
    public static void main(String[] args) {
        Employee emp = new Employee("John", 15.50, 40);
        Employee mng = new Manager("Mark", 25.6, 35, 500);
        System.out.println(emp.getName() + "'s paycheque is: $" + emp.calculatePay());
        System.out.println(mng.getName() + "'s paycheque is: $" + mng.calculatePay());
    }
}
