/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employee;

/**
 *
 * @author jaiminlakhani
 */
public class EmployeeFactory {
    private static EmployeeFactory factory;
    
    private EmployeeFactory() {}
    
    public static EmployeeFactory getInstance() {
        if(factory == null) {
            factory = new EmployeeFactory();
        }
        return factory;
    }
    
}

